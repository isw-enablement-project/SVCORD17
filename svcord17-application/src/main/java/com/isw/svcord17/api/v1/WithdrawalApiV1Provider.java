package com.isw.svcord17.api.v1;

import com.isw.svcord17.domain.svcord.service.WithdrawalDomainService;
import com.isw.svcord17.sdk.api.v1.api.WithdrawalApiV1Delegate;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import com.isw.svcord17.sdk.api.v1.model.ErrorSchema;
import com.isw.svcord17.sdk.api.v1.model.WithdrawalBodySchema;
import com.isw.svcord17.sdk.api.v1.model.WithdrawalResponseSchema;
import com.isw.svcord17.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord17.sdk.domain.svcord.entity.WithdrawalDomainServiceInput;
import com.isw.svcord17.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * A stub that provides implementation for the WithdrawalApiV1Delegate
 */
@SuppressWarnings("unused")
@Service
@ComponentScan(basePackages = "com.isw.svcord17.sdk.api.v1.api")
public class WithdrawalApiV1Provider implements WithdrawalApiV1Delegate {

  
  @Autowired
  WithdrawalDomainService withdrawaDomainService;

  @Autowired
  DomainEntityBuilder entityBuilder;

  @Override
  public ResponseEntity<WithdrawalResponseSchema> servicingOrderWithdrawalPost(WithdrawalBodySchema withdrawalBodySchema) {
    //TODO Auto-generated method stub
   
    WithdrawalResponseSchema withdrawalResponseSchema = new WithdrawalResponseSchema();
    WithdrawalDomainServiceInput withdrawalDomainServiceInput = entityBuilder.getSvcord()
    .getWithdrawalDomainServiceInput()
    .setAccountNumber(withdrawalBodySchema.getAccountNumber())
    .setAmount(withdrawalBodySchema.getAmount())
    .build();
    
    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput;
    withdrawalDomainServiceOutput = withdrawaDomainService.execute(withdrawalDomainServiceInput);

    withdrawalResponseSchema.setTransactionID(withdrawalDomainServiceOutput.getTrasactionId());
    withdrawalResponseSchema.setServicingOrderWorkTaskResult(withdrawalDomainServiceOutput.getServicingOrderWorkResult().toString());

    return ResponseEntity.ok(null);
  }

}
