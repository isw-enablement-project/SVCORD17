package com.isw.svcord17.domain.svcord.command;


import org.springframework.stereotype.Service;
import com.isw.svcord17.sdk.domain.svcord.command.Svcord17CommandBase;
import com.isw.svcord17.sdk.domain.svcord.entity.Svcord17Entity;
import com.isw.svcord17.sdk.domain.svcord.entity.ThirdPartyReferenceEntity;
import com.isw.svcord17.sdk.domain.svcord.type.ServicingOrderType;
import com.isw.svcord17.sdk.domain.svcord.type.ServicingOrderWorkProduct;
import com.isw.svcord17.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord17.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord17.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord17Command extends Svcord17CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord17Command.class);

  public Svcord17Command(DomainEntityBuilder entityBuilder,  Repository repo ) {
    super(entityBuilder,  repo );
  }

  
    
    @Override
    public com.isw.svcord17.sdk.domain.svcord.entity.Svcord17 createServicingOrderProducer(com.isw.svcord17.sdk.domain.svcord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput)  {
    

      log.info("Svcord17Command.createServicingOrderProducer()");
      // TODO: Add your command implementation logic

      Svcord17Entity servicingOrderProcedure = this.entityBuilder.getSvcord().getSvcord17().build();
      servicingOrderProcedure.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessStartDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS);
      servicingOrderProcedure.setServicingOrderWorkDescription("CASH WITHDRAWALS");
      servicingOrderProcedure.setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT);
      servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING);
      
      ThirdPartyReferenceEntity thirdPartyReference = new ThirdPartyReferenceEntity();
      thirdPartyReference.setId("test1");
      thirdPartyReference.setPassword("password");
      servicingOrderProcedure.setThirdPartyReference(thirdPartyReference);
   
      return repo.getSvcord().getSvcord17().save(servicingOrderProcedure);
    }
  
    
    @Override
    public void updateServicingOrderProducer(com.isw.svcord17.sdk.domain.svcord.entity.Svcord17 instance, com.isw.svcord17.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput)  { 

      log.info("Svcord17Command.updateServicingOrderProducer()");
      // TODO: Add your command implementation logic
      Svcord17Entity servicingOrderProcedure = this.repo.getSvcord().getSvcord17().getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
      servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessEndDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());
      servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());
    
      log.info(updateServicingOrderProducerInput.getUpdateID().toString());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAccountNumber());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAmount());
    
      this.repo.getSvcord().getSvcord17().save(servicingOrderProcedure);
    
      
    }
  
}
